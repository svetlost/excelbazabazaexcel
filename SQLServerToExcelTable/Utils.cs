﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelToDbDbToExcell
{
    class Utils
    {
        public TextBox lb = new TextBox();
        string dataSource, db;
        Label serverLabel, DataBaseLabel;
        SqlConnection cnn;
        string localIp;

        public Utils(TextBox _lb, Label _serverLabel, Label _DataBaseLabel, string _dataSource, string _db, SqlConnection _cnn)
        {
            lb = _lb;
            DataBaseLabel = _DataBaseLabel;
            serverLabel = _serverLabel;
            dataSource = _dataSource;
            db = _db;
            cnn = _cnn;
            localIp = GetMyIP();
        }

        public bool IsAdministrator()
        {
            bool a =
            (new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                      .IsInRole(WindowsBuiltInRole.Administrator);
            return a;
        }

        public void DisplayData(System.Data.DataTable table, string db = "")
        {
            if (table.Rows.Count == 0) 
            {
                lb.AppendText("\r\nNo Tables present");
                lb.AppendText("\r\nNothing to Export");
                return;                
            }
            foreach (System.Data.DataRow row in table.Rows)
            {

                lb.AppendText("\r\n" + row["TABLE_NAME"].ToString());                
                lb.AppendText("\r\n============================");
               
            }
            lb.AppendText($"\r\n SUCCESFULLY CONNECTED TO DataBase: {db}");
           
        }

        public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                lb.AppendText("\r\n");
                lb.AppendText("Exception Occurred while releasing object: " + ex.Message);
                lb.AppendText("\r\n");
            }
            finally
            {
                GC.Collect();
            }
        }

        public SqlConnection Connection(string connectionString,bool notmaster=true)
        {
            lb.AppendText("\r\n");
            lb.AppendText("\r\nTRYING TO CONNECT TO SERVER.... PLEASE WAIT");
            lb.AppendText("\r\n");
            try
            {
                if (cnn != null && cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                    cnn.Dispose();
                }

                cnn = new SqlConnection(connectionString);
                cnn.Open();

                lb.AppendText("\r\nSUCCESFULLY CONNECTED TO SERVER");
                lb.AppendText("\r\n");

                var builder = new SqlConnectionStringBuilder(connectionString);
                dataSource = builder.DataSource;
                db = builder.InitialCatalog;
                serverLabel.Text = dataSource;
                DataBaseLabel.Text = db;

                if (notmaster)
                {
                    DataTable t = cnn.GetSchema("Tables");
                    lb.AppendText($"\r\nTABLES in DataBase {db}:");
                    DisplayData(t,db);
                    lb.AppendText("\r\n");
                }

                return cnn;
            }

            catch (SqlException ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText(ex.Message);
                lb.AppendText("\r\n");
                lb.AppendText("\r\nCheck if database exists");
                return null;
            }
            catch (Exception ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText(ex.Message);
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                lb.AppendText(ex.ToString());
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                return null;
            }
        }

        public  bool GrantAccess(string fullPath)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(fullPath);
                WindowsIdentity self = System.Security.Principal.WindowsIdentity.GetCurrent();
                DirectorySecurity ds = info.GetAccessControl();
                ds.AddAccessRule(new FileSystemAccessRule(self.Name,
                FileSystemRights.FullControl,
                InheritanceFlags.ObjectInherit |
                InheritanceFlags.ContainerInherit,
                PropagationFlags.None,
                AccessControlType.Allow));
                info.SetAccessControl(ds);
                return true;
            }
            catch (IOException ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText(ex.Message);
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                lb.AppendText(ex.ToString());
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                return false;
            }

            catch (Exception ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText(ex.Message);
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                lb.AppendText(ex.ToString());
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                return false;
            }
        }

        public bool CheckDatabaseExists(string connection, string databaseName)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                using (SqlConnection tmpConn = new SqlConnection(connection))
                {
                    sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);
                    using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                    {
                        tmpConn.Open();
                        object resultObj = sqlCmd.ExecuteScalar();
                        int databaseID = 0;
                        if (resultObj != null)
                        {
                            int.TryParse(resultObj.ToString(), out databaseID);
                        }
                        tmpConn.Close();
                        result = (databaseID > 0);
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        // No point of passing a bool if all you do is return it...
        public bool CheckDatabase(string serverName, string databaseName, bool returnDbList=false)
        {

            var connString = "Server=" + serverName + ";Integrated Security=SSPI;database=master";
            var cmdText = "select count(*) from master.dbo.sysdatabases where name=@database";
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    using (var sqlCmd = new SqlCommand(cmdText, sqlConnection))
                    {
                        // Use parameters to protect against Sql Injection
                        sqlCmd.Parameters.Add("@database", System.Data.SqlDbType.NVarChar).Value = databaseName;

                        // Open the connection as late as possible
                        sqlConnection.Open();
                        // count(*) will always return an int, so it's safe to use Convert.ToInt32
                       


                        if (returnDbList)
                        {
                            lb.AppendText("\r\n");
                            lb.AppendText("\r\nList of existing databases: ");
                            // Set up a command with the given query and associate
                            // this with the current connection.
                            using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", sqlConnection))
                            {
                                using (IDataReader dr = cmd.ExecuteReader())
                                {
                                    while (dr.Read())
                                    {
                                        lb.AppendText("\r\n" + dr[0].ToString());
                                    }
                                }
                            }                           
                        }

                        return Convert.ToInt32(sqlCmd.ExecuteScalar()) == 1;
                    }                    
                }
            }
            catch (Exception ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText("\r\n");
                lb.AppendText(ex.Message);
                lb.AppendText("\r\n");
                lb.AppendText($"\r\nYour Local ip address is: {localIp},1433 , the address  stated in configuration file is {dataSource}");
                lb.AppendText("\r\n");
                lb.AppendText("\r\nTROUBLESHOOTING:"+
                    "\r\nIn order to create database you have to be connected to server" +
                    "\r\nCheck your ip address:"+
                    "\r\n-if this is first time you connect, this is not a problem"+
                    "\r\nCheck TCP :"+
                    "\r\nTCP Protocol is used for connecting to server:" +
                    "\r\nGoto Start -> Programs -> Microsoft SQL ServerYYYY ->  SQL Server YYYY Configuration Manager or run SQLServerManager12.msc." +
                    "\r\nMake sure that TCP / IP is enabled under SQL Native Client Configuration ->  Client Protocols.Then go into SQL Server Network Configuration and double click TCP / IP." +
                    "\r\nClick the IP Addresses tab and scroll to the bottom." +
                    "\r\nUnder IP All remove TCP Dynamic Ports if it is present and set TCP Port to 1433." +
                    "\r\nClick OK and restart your computer.");
                return false;

            }

        }

        public string GetMyIP()
        {           
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                return endPoint.Address.ToString();
            }
        }
        public bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
    }


    public static class DataTableExt
    {
        /// <summary>
        /// For specific conversion of excel table column type to correct db type
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <param name="newType"></param>
        public static void ConvertColumnType(this DataTable dt, string columnName, Type newType)
        {
            using (DataColumn NewColumn = new DataColumn(columnName + "_new", newType))
            {
                // Add the new column which has the new type, and move it to the ordinal of the old column
                int ordinal = dt.Columns[columnName].Ordinal;
                dt.Columns.Add(NewColumn);
                NewColumn.SetOrdinal(ordinal);

                // Get and convert the values of the old column, and insert them into the new
                foreach (DataRow dr in dt.Rows)
                {

                    if (newType == typeof(System.Guid))
                    {
                        Guid x = new Guid(dr[columnName].ToString());

                        dr[NewColumn.ColumnName] = x;
                    }
                    else
                    {
                        dr[NewColumn.ColumnName] = Convert.ChangeType(dr[columnName], newType);
                    }
                }
                // Remove the old column
                dt.Columns.Remove(columnName);

                // Give the new column the old column's name
                NewColumn.ColumnName = columnName;
            }
        }



        
    }
}
