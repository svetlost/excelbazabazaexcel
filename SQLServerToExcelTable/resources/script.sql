/****** Object:  Table [dbo].[code_words]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[code_words];


CREATE TABLE [dbo].[code_words](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[command_code] [varchar](30) NOT NULL,
	[code_description] [varchar](30) NOT NULL,
	[number_of_bits] [tinyint] NOT NULL,
	[cyclic_read] [tinyint] NOT NULL,
	[code_order_in_regulator] [tinyint] NULL,
	[par_name_in_app] [varchar](30) NULL	
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[motor_list]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[motor_list];

CREATE TABLE [dbo].[motor_list](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[screen_order_number] [tinyint] NOT NULL,
	[title] [varchar](45) NOT NULL,
	[ip_address] [varchar](45) NOT NULL,
	[sbc_ip_address] [varchar](45) NOT NULL,
	[sbc_modul] [tinyint] NOT NULL,
	[init_sbc_bit] [tinyint] NOT NULL,
	[sbc_bit] [tinyint] NOT NULL,
	[name_3d] [int] NOT NULL,
	[max_v] [float] NOT NULL,
	[default_mv] [float] NOT NULL,
	[lim_neg] [float] NOT NULL,
	[lin_pos] [float] NOT NULL,
	[limit_coefficient] [float] NOT NULL,
	[enabled] [bit] NOT NULL,
	[used] [bit] NOT NULL,
	[inverted] [bit] NOT NULL,
	[modbus] [bit] NOT NULL,
	[coefficient] [float] NOT NULL,
	[cluster_logical] [tinyint] NOT NULL,
	[graph_min] [float] NOT NULL,
	[graph_max] [float] NOT NULL,
	[cabinet_title] [varchar](45) NOT NULL,
	[loadcell_zeroload] [float] NOT NULL,
	[loadcell_ratedload] [float] NOT NULL,
	[loadcell_overload] [float] NOT NULL,
	[loadcell_underload] [float] NOT NULL,
	[loadcell_overload_enabled] [bit] NOT NULL,
	[loadcell_underload_enabled] [bit] NOT NULL,
	[loadcell_enabled] [bit] NULL,
	[sp_coefficient] [float] NOT NULL,
	[mv_sv_coefficient] [float] NOT NULL,
	[cv_coefficient] [float] NOT NULL,
	[cluster_electric] [tinyint] NOT NULL,
	[big_ticks] [float] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[password]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[password];

CREATE TABLE [dbo].[password](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_password] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ph_joystick]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[ph_joystick];

CREATE TABLE [dbo].[ph_joystick](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[j_group] [tinyint] NOT NULL,
	[j_index] [tinyint] NOT NULL,
	[inverted] [bit] NOT NULL,
	[j_guid] [uniqueidentifier] NOT NULL,
	[j_measured_min] [float] NOT NULL,
	[j_measured_max] [float] NOT NULL,
	[j_measured_mid] [float] NOT NULL,
	[j_dead_zone] [float] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rfid]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[rfid];

CREATE TABLE [dbo].[rfid](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rfid_tag] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[up_down_control]    Script Date: 3/6/2020 3:34:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP TABLE IF EXISTS [dbo].[up_down_control];
CREATE TABLE [dbo].[up_down_control](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[motor_id] [int] NOT NULL,
	[m_row] [tinyint] NOT NULL,
	[m_column] [tinyint] NOT NULL,
	[net_address] [int] NOT NULL
) ON [PRIMARY]
GO