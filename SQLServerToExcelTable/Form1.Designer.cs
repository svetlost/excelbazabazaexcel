﻿namespace SQLServerToExcelTable
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.createExcel = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.importExcel = new System.Windows.Forms.Button();
            this.txtpath = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dbConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.selectXlsFile = new System.Windows.Forms.Button();
            this.dbName = new System.Windows.Forms.TextBox();
            this.btnCreateDatabase = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listOfDBs = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.serverLabel = new System.Windows.Forms.Label();
            this.DataBaseLabel = new System.Windows.Forms.Label();
            this.infoTextBox = new System.Windows.Forms.TextBox();
            this.help = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // createExcel
            // 
            this.createExcel.Location = new System.Drawing.Point(72, 46);
            this.createExcel.Margin = new System.Windows.Forms.Padding(2);
            this.createExcel.Name = "createExcel";
            this.createExcel.Size = new System.Drawing.Size(117, 52);
            this.createExcel.TabIndex = 7;
            this.createExcel.Text = "Create Excel Book From DB";
            this.createExcel.UseVisualStyleBackColor = true;
            this.createExcel.Click += new System.EventHandler(this.ExportToExcelTable_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(489, 15);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(639, 71);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 79;
            this.pictureBox3.TabStop = false;
            // 
            // importExcel
            // 
            this.importExcel.Location = new System.Drawing.Point(72, 133);
            this.importExcel.Margin = new System.Windows.Forms.Padding(2);
            this.importExcel.Name = "importExcel";
            this.importExcel.Size = new System.Drawing.Size(117, 36);
            this.importExcel.TabIndex = 80;
            this.importExcel.Text = "Import Excel File to DB";
            this.importExcel.UseVisualStyleBackColor = true;
            this.importExcel.Click += new System.EventHandler(this.importExcel_Click);
            // 
            // txtpath
            // 
            this.txtpath.AcceptsTab = true;
            this.txtpath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtpath.Location = new System.Drawing.Point(5, 32);
            this.txtpath.Margin = new System.Windows.Forms.Padding(2);
            this.txtpath.Name = "txtpath";
            this.txtpath.Size = new System.Drawing.Size(259, 20);
            this.txtpath.TabIndex = 81;
            this.txtpath.Text = "excel file name";
            // 
            // dbConnect
            // 
            this.dbConnect.Location = new System.Drawing.Point(89, 32);
            this.dbConnect.Name = "dbConnect";
            this.dbConnect.Size = new System.Drawing.Size(117, 42);
            this.dbConnect.TabIndex = 84;
            this.dbConnect.Text = "CONNECT TO DB from settings string";
            this.toolTip1.SetToolTip(this.dbConnect, "Connect to Database\r\n\r\nConnection is set in properties");
            this.dbConnect.UseVisualStyleBackColor = true;
            this.dbConnect.Click += new System.EventHandler(this.dbConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.createExcel);
            this.groupBox1.Location = new System.Drawing.Point(17, 289);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 156);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Export data from DataBase to Excel";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.selectXlsFile);
            this.groupBox2.Controls.Add(this.importExcel);
            this.groupBox2.Controls.Add(this.txtpath);
            this.groupBox2.Location = new System.Drawing.Point(17, 465);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(269, 186);
            this.groupBox2.TabIndex = 86;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Import Data to DataBase from Excel";
            // 
            // selectXlsFile
            // 
            this.selectXlsFile.Location = new System.Drawing.Point(72, 57);
            this.selectXlsFile.Name = "selectXlsFile";
            this.selectXlsFile.Size = new System.Drawing.Size(117, 38);
            this.selectXlsFile.TabIndex = 89;
            this.selectXlsFile.Text = "Select .xls File";
            this.selectXlsFile.UseVisualStyleBackColor = true;
            this.selectXlsFile.Click += new System.EventHandler(this.FileBrowse_Click);
            // 
            // dbName
            // 
            this.dbName.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.dbName.Location = new System.Drawing.Point(5, 29);
            this.dbName.Margin = new System.Windows.Forms.Padding(2);
            this.dbName.Name = "dbName";
            this.dbName.Size = new System.Drawing.Size(259, 20);
            this.dbName.TabIndex = 91;
            this.dbName.Text = "new or existing database  name";
            // 
            // btnCreateDatabase
            // 
            this.btnCreateDatabase.Location = new System.Drawing.Point(72, 54);
            this.btnCreateDatabase.Name = "btnCreateDatabase";
            this.btnCreateDatabase.Size = new System.Drawing.Size(117, 44);
            this.btnCreateDatabase.TabIndex = 90;
            this.btnCreateDatabase.Text = "New DataBase  Or Connect Existing";
            this.btnCreateDatabase.UseVisualStyleBackColor = true;
            this.btnCreateDatabase.Click += new System.EventHandler(this.btnCreateDatabase_Click);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox2.Location = new System.Drawing.Point(287, 92);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(1442, 559);
            this.textBox2.TabIndex = 88;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "Select Excel file";
            this.openFileDialog.Filter = "Text files (*.xls)|*.xls";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listOfDBs);
            this.groupBox3.Controls.Add(this.dbName);
            this.groupBox3.Controls.Add(this.btnCreateDatabase);
            this.groupBox3.Location = new System.Drawing.Point(17, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(269, 181);
            this.groupBox3.TabIndex = 92;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Create New or Connect to Existing DataBase ";
            // 
            // listOfDBs
            // 
            this.listOfDBs.Location = new System.Drawing.Point(72, 130);
            this.listOfDBs.Name = "listOfDBs";
            this.listOfDBs.Size = new System.Drawing.Size(117, 45);
            this.listOfDBs.TabIndex = 92;
            this.listOfDBs.Text = "List Of Existing DataBases";
            this.listOfDBs.UseVisualStyleBackColor = true;
            this.listOfDBs.Click += new System.EventHandler(this.listOfDBs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(284, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 18);
            this.label1.TabIndex = 93;
            this.label1.Text = "Currently Connected To:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(284, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 94;
            this.label2.Text = "Server:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(284, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 95;
            this.label3.Text = "DataBase: ";
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.serverLabel.Location = new System.Drawing.Point(371, 50);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(46, 18);
            this.serverLabel.TabIndex = 96;
            this.serverLabel.Text = "label4";
            // 
            // DataBaseLabel
            // 
            this.DataBaseLabel.AutoSize = true;
            this.DataBaseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DataBaseLabel.Location = new System.Drawing.Point(371, 68);
            this.DataBaseLabel.Name = "DataBaseLabel";
            this.DataBaseLabel.Size = new System.Drawing.Size(46, 18);
            this.DataBaseLabel.TabIndex = 97;
            this.DataBaseLabel.Text = "label4";
            // 
            // infoTextBox
            // 
            this.infoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.infoTextBox.Location = new System.Drawing.Point(1133, 16);
            this.infoTextBox.Multiline = true;
            this.infoTextBox.Name = "infoTextBox";
            this.infoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.infoTextBox.Size = new System.Drawing.Size(596, 70);
            this.infoTextBox.TabIndex = 98;
            // 
            // help
            // 
            this.help.Location = new System.Drawing.Point(287, 6);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(162, 23);
            this.help.TabIndex = 99;
            this.help.Text = "How to install SQL server ";
            this.help.UseVisualStyleBackColor = true;
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1738, 659);
            this.Controls.Add(this.help);
            this.Controls.Add(this.DataBaseLabel);
            this.Controls.Add(this.serverLabel);
            this.Controls.Add(this.infoTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dbConnect);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox3);
            this.HelpButton = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "SQL Server <--> Excel";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button createExcel;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button importExcel;
        private System.Windows.Forms.TextBox txtpath;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button dbConnect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button selectXlsFile;
        private System.Windows.Forms.TextBox dbName;
        private System.Windows.Forms.Button btnCreateDatabase;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.Label DataBaseLabel;
        private System.Windows.Forms.TextBox infoTextBox;
        private System.Windows.Forms.Button listOfDBs;
        private System.Windows.Forms.Button help;
    }
}

