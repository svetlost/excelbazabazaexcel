﻿
using ExcelToDbDbToExcell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace SQLServerToExcelTable
{
    public partial class Form1 : Form
    {
        TextBox lb = new TextBox();
        Utils Util;
        static string connectionString = ExcelToDbDbToExcell.Properties.Settings.Default.SQLServerConnectionString;
        string dataSource, db;
        SqlConnection cnn;
        string ConnectionStringForImport;
        public Form1()
        {


            InitializeComponent();
            ConfigurationManager.RefreshSection("connectionStrings");
            lb = textBox2;
            infoTextBox.Text = "This application is to be used only on SERVER machine" +
                "\r\nRUN THIS APP AS ADMINISTRATOR" +
                "\r\nIt is used for connecting or setting new databases and tables on Microsoft SQL Server." +
                "\r\nSQL Server is application accessible through ip address of the computer where it is installed," +
                "\r\n it has DataBases, database has tables similar to excel sheets" +
                "\r\nData is in tables"+
                "\r\nTo work with databases we have to be connected to the server." +
                "\r\nServer connection parameter from application settings is used for connecting to server." +
                "\r\nYou can create new database "+
                "\r\nDatabases are created on SQL server. You can't create database if not connected to server" +
                "\r\nPlease note that new database can be created only on computer where the Microsoft SQL server is installed";
            textBox2.ScrollBars = ScrollBars.Vertical;
            serverLabel.Text = "not connected";
            DataBaseLabel.Text = "not connected";           
            var builder = new SqlConnectionStringBuilder(connectionString);
            dataSource = builder.DataSource;
            db = builder.InitialCatalog;
            bool conn = builder.IntegratedSecurity;
            Util = new Utils(lb, serverLabel, DataBaseLabel, dataSource, db, cnn);
            lb.AppendText($"These are current SETTINGS (from ExcelToDbDbToExcell.exe.config file, located in install dir) used for connecting to sql server and database :\n");
            lb.AppendText("\r\n");
            lb.AppendText($"\r\nSQL SERVER IP ADDRESS,PORT (data source):{dataSource}\n");
            lb.AppendText($"\r\nDATABASE NAME (initial catalog):  {db}\n");
            lb.AppendText($"\r\nCONNECTION TYPE:");
            if (conn) { lb.AppendText(" No user name or password are used (windows authentication).Can be used only on local computer. \n"); }
            else { lb.AppendText(" User name and password authentication \n"); }

            if (Util.CheckDatabase(dataSource, db))
            {
                lb.AppendText($"\r\nSQL SERVER STATUS:  NOT CONNECTED\n");
                lb.AppendText($"\r\nDATABASE STATUS:  {db} Exists \n");
                lb.AppendText("\r\n");
                lb.AppendText("\r\n!!!CONNECTION POSSIBLE!!! --- please click on Connect to DB from settings string ");
                lb.AppendText("\r\n");
                lb.AppendText($"\r\nTo export or import data to and from {db} database click CONNECT TO DB...  button\n");
                lb.AppendText("\r\n *This will connect to server and database stated in connection string*");
                lb.AppendText("\r\n");
                lb.AppendText($"\r\n If this is setup procedure and you haven't created new database, or you need different database to work with, click on new database... button\n");
                lb.AppendText($"\r\nRemember to change to new database name (initial catalog) in settings file\n");

            }
            else
            {
                lb.AppendText("\r\n");
                lb.AppendText($"\r\nDATABASE ERROR :  {db} Does not Exist! \r\nPlease check your database name (initial catalog) in properties");
                lb.AppendText("\r\n");
                lb.AppendText($"\r\n If this is setup procedure, give your database a name and click on New DataBase... button\n");
            }

            if (Util.IsAdministrator() == false) {
                lb.AppendText("\r\n");
                lb.AppendText($"\r\nYOU MUST RUN AS ADMINISTRATOR!!!");
                lb.AppendText($"\r\nCreating of new datatbases won't be allowed");
                lb.AppendText($"\r\nImporting to existing database is allowed");
            }
        }



        private void ExportToExcelTable_Click(object sender, EventArgs e)
        {
            if (db.Equals("master")) { lb.AppendText("\r\nDO NOT USE MASTER DataBase, Create New or select different!"); return; }
            string sql = null;
            string data = null;
            int i = 0;
            int j = 0;
            int a = 0;

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            if (cnn == null)
            {
                lb.AppendText("\r\n");
                lb.AppendText("\r\nYou are not connected to server or any database");
                lb.AppendText("\r\n");
                return;
            }
            if(cnn.State == ConnectionState.Closed) {
               // Util.Connection(); 
            }
            DataTable t = cnn.GetSchema("Tables");
            lb.AppendText($"\r\nExporting These tables from DataBase:{db}");
            Util.DisplayData(t,db);

            SqlDataAdapter dscmd;
            DataSet ds;
            int counter = 1;

            xlWorkBook = xlApp.Workbooks.Add(misValue);

            foreach (System.Data.DataRow row in t.Rows)
            {
                xlWorkSheet = xlWorkBook.Sheets.Add(misValue, misValue, 1, misValue) as Excel.Worksheet;
                counter++;

                xlWorkSheet.Name = row["TABLE_NAME"].ToString();

                sql = "SELECT * FROM " + row["TABLE_NAME"].ToString();
                dscmd = new SqlDataAdapter(sql, cnn);
                ds = new DataSet();
                dscmd.Fill(ds);

                for (a = 0; a <= ds.Tables[0].Columns.Count - 1; a++)
                {
                    data = ds.Tables[0].Columns[a].ToString();
                    xlWorkSheet.Cells[1, a + 1] = data;
                }

                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    for (j = 0; j <= ds.Tables[0].Columns.Count - 1; j++)
                    {
                        data = ds.Tables[0].Rows[i].ItemArray[j].ToString();
                        xlWorkSheet.Cells[i + 2, j + 1] = data;
                    }
                }

                Util.releaseObject(xlWorkSheet);

            }

            string filename = string.Format(@"c:\dbDump-{0:dd-MM-yyyy_HH-mm-ss}.xls", DateTime.Now);

            xlWorkBook.Sheets["Sheet1"].Delete();

            xlWorkBook.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();


            Util.releaseObject(xlWorkBook);
            Util.releaseObject(xlApp);
            lb.AppendText("\r\n");
            lb.AppendText("\r\nExcel file created , you can find the file at " + filename);
            // Clean up.
            if (cnn != null)
            {
                cnn.Close();
                cnn.Dispose();
            }
        }



        private void dbConnect_Click(object sender, EventArgs e)
        {
            if (db.Equals("master")) {
                lb.AppendText("\r\n"); lb.AppendText("\r\nDO NOT USE MASTER DataBase, Create New or select different!"); return; }
            cnn = Util.Connection(connectionString);
            if (cnn == null)
            {
               bool connectionStatus= Util.CheckDatabase(dataSource, db,true);
                if (connectionStatus == false)
                {
                    lb.AppendText("\r\n");
                    lb.AppendText($"\r\n You can connect to server,which is excellent, but there is no database {db}");
                    lb.AppendText($"\r\nDATABASE ERROR :  {db} Does not Exist! \r\nPlease check your database name (initial catalog) in settings file");
                    lb.AppendText("\r\n");
                    lb.AppendText($"\r\n If this is setup procedure, give your database a name and click on create new... button\n");

                }
            }
        }




        private void importExcel_Click(object sender, EventArgs e)
        {
            
            if (ConnectionStringForImport == null)
            {
                ConnectionStringForImport = connectionString;
            }
            cnn = Util.Connection(ConnectionStringForImport);
            if (db.Equals("master")) { lb.AppendText("\r\nDO NOT USE MASTER DataBase, Create New or select different!"); return; }
            lb.AppendText("\r\n");
            lb.AppendText("\r\nChecking server connection, database and tables");
            if (cnn == null)
            {
                lb.AppendText("\r\n");
                lb.AppendText("\r\nThere is no connection to server");
                lb.AppendText("\r\nUse connect to db from settings button or create new database... button");
                return;
            }

            var confirmResult = MessageBox.Show("THIS WILL OVERWRITE ALL DATABASE DATA WITH EXCEL VALUES",
                                     "Confirm",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.No)
            {
                return;
            }


            char[] MyChar = { ' ', '$' };
            OleDbConnection ExcelCon = null;
            DataTable dtSheet = null;
            List<string> ListOfSheet = new List<string>();
            try
            {
               
                string _path = txtpath.Text;
                if (_path.Equals("excel file name")) { lb.AppendText("\r\n"); lb.AppendText("\r\nPlease Select .xls File first\r\n"); return; }
                string constr = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0}; Extended Properties='Excel 8.0;IMEX=1;'", _path);
                ExcelCon = new OleDbConnection(constr);

                ExcelCon.Open();

                // Get the data table congaing the schema guid.
                dtSheet = ExcelCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
               

                lb.AppendText("\r\nImporting data to table:");
                foreach (DataRow drSheet in dtSheet.Rows)
                {
                    if (drSheet["TABLE_NAME"].ToString().Contains("$"))//checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
                    {
                        // ListOfSheet.Add(drSheet["TABLE_NAME"].ToString().Trim(MyChar));

                        string Query = string.Format("Select * FROM [{0}]", drSheet["TABLE_NAME"].ToString());

                        DataSet excelSheet = new DataSet();
                        OleDbDataAdapter oda = new OleDbDataAdapter(Query, ExcelCon);
                        oda.Fill(excelSheet);

                        DataTable Exceldt = excelSheet.Tables[0];

                        lb.AppendText($"\r\nExcel --> Rows in sheet {drSheet["TABLE_NAME"].ToString().Trim(MyChar)} : {Exceldt.Rows.Count.ToString()}");
                        lb.AppendText("\r\n");
                        //converting to correct type
                        if (drSheet["TABLE_NAME"].ToString().Equals("ph_joystick$"))
                        {
                            Exceldt.ConvertColumnType("j_guid", typeof(System.Guid));
                        }
                        if (drSheet["TABLE_NAME"].ToString().Equals("code_words$"))
                        {
                            for (int i = 0; i < Exceldt.Rows.Count; i++)
                            {
                                string columnValue = Exceldt.Rows[i]["command_code"].ToString();
                                if (!columnValue.Contains("."))
                                {
                                    lb.AppendText("\r\n");
                                    lb.AppendText($"\r\nWARNING: command_code row {i+1} in excel table code_words with value {columnValue} does not contain dot(.) separator");
                                    lb.AppendText("\r\nDataBase continued on importing...this will cause error in modbus_server ");
                                   
                                }
                                if((columnValue.Length - columnValue.IndexOf(".", StringComparison.CurrentCulture) - 1) != 3)
                                {
                                   
                                    lb.AppendText($"\r\nWARNING: command_code row {i + 1} in excel table code_words with value {columnValue} does not have three digits after dot(.) separator");
                                    lb.AppendText("\r\nDataBase continued on importing...this will cause error in modbus_server");
                                    lb.AppendText("\r\n");
                                }

                            }
                        }


                        List<string> columns = new List<string>();
                        using (SqlCommand com = new SqlCommand(@"SELECT* FROM "+ drSheet["TABLE_NAME"].ToString().Trim(MyChar), cnn))
                        {
                            
                            using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.SchemaOnly))
                            {
                                DataTable schemaTable = reader.GetSchemaTable();
                                foreach (DataRow colRow in schemaTable.Rows)
                                    columns.Add(colRow.Field<String>("ColumnName"));
                            }

                        }

                            //empty table
                            SqlCommand truncate = new SqlCommand("TRUNCATE TABLE " + drSheet["TABLE_NAME"].ToString().Trim(MyChar), cnn);
                        truncate.ExecuteNonQuery();

                        using (SqlTransaction transaction = cnn.BeginTransaction())
                        {
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(cnn, SqlBulkCopyOptions.KeepIdentity, transaction))
                            {
                                bulkCopy.BatchSize = 10;
                                bulkCopy.DestinationTableName = drSheet["TABLE_NAME"].ToString().Trim(MyChar);  
                                



                                foreach (DataColumn dc in Exceldt.Columns)
                                {
                                    bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                                   var x = columns.FirstOrDefault(q=> q==dc.ColumnName);
                                    if (x == null)
                                    {
                                        lb.AppendText($"\r\nIn SQL table {bulkCopy.DestinationTableName} there is no column with name : " + dc.ColumnName);
                                    }
                                    
                                }
                                

                                try
                                {
                                    bulkCopy.WriteToServer(Exceldt);
                                    transaction.Commit();
                                }
                                catch (Exception ex)
                                {
                                    lb.AppendText($"\r\n"+ex.Message);
                                    transaction.Rollback();
                                }
                                finally
                                {
                                    ExcelCon.Close();
                                }
                            }
                        }
                        string stmt = "SELECT COUNT(*) FROM dbo."+ drSheet["TABLE_NAME"].ToString().Trim(MyChar);
                        int count = 0;
                        using (SqlCommand cmdCount = new SqlCommand(stmt, cnn))
                        {                            
                            count = (int)cmdCount.ExecuteScalar();
                            lb.AppendText($"\r\nSQL --> Imported Rows in {drSheet["TABLE_NAME"].ToString().Trim(MyChar)} : {count}");
                            lb.AppendText("\r\n");
                        }
                    }
                }
                lb.AppendText("\r\n");
                lb.AppendText("\r\nimport to DB Successful... ");

            }
            catch (Exception ex)
            {
                lb.AppendText("\r\n");
                lb.AppendText("\r\n" + ex.Message);
                lb.AppendText("\r\n");

                return;
            }
            finally
            {
                // Clean up.
                if (ExcelCon != null)
                {
                    ExcelCon.Close();
                    ExcelCon.Dispose();
                }
                if (dtSheet != null)
                {
                    dtSheet.Dispose();
                }
               
            }

        }
        

        private void FileBrowse_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                
                try
                {
                    FileInfo fi = new FileInfo(openFileDialog.FileName);
                    if(Util.IsFileLocked(fi))
                    {
                        lb.AppendText("\r\n");
                        lb.AppendText($"\r\nFile is open, most probably in excel!");
                        return;
                    }
                    var sr = new StreamReader(openFileDialog.FileName);
                    txtpath.Text = openFileDialog.FileName;
                }
                catch (SecurityException ex)
                {
                    lb.AppendText("\r\n");
                    lb.AppendText($"\r\nSecurity error.\r\nError message: {ex.Message}");


                }
            }
        }

        private void listOfDBs_Click(object sender, EventArgs e)
        {
            Util.CheckDatabase(dataSource, "master", true);
        }

        private void help_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            System.Diagnostics.Process.Start(path+"\\resources\\help.html");
        }

        private void btnCreateDatabase_Click(object sender, EventArgs e)
        {
            String CreateDatabase;
            string connectMaster = "Server=" + dataSource + ";database=master;Trusted_Connection=Yes";

            String str;
            bool nameError = (dbName.Text.Equals("new or existing database  name"));
            if (nameError) { lb.AppendText("\r\n"); lb.AppendText("\r\nPlese input DataBase name"); }

            bool nameError1 = (dbName.Text.Equals("database"));
            if (nameError1) { lb.AppendText("\r\n"); lb.AppendText("\r\nYou can't use word dtatabase, it's reserved"); }

            bool ContainsEmpty = (dbName.Text.Contains(" "));
            if (ContainsEmpty) { lb.AppendText("\r\n"); lb.AppendText("\r\nNo spaces in name allowed"); }

            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            bool regexError = (!regexItem.IsMatch(dbName.Text));
            if (regexError) { lb.AppendText("\r\n"); lb.AppendText("\r\nOnly letters and Numbers allowed"); }



            if (nameError || ContainsEmpty || regexError) { return; }
            lb.AppendText("\r\n");
            lb.AppendText($"\r\nChecking if database {dbName.Text} is available:");


            bool IsDBExits = Util.CheckDatabaseExists(connectMaster, dbName.Text); //Check database exists in sql server.
            if (IsDBExits)
            {
                lb.AppendText("\r\n");
                lb.AppendText("\r\nData Base with that name exists, please use some other name");
                ConnectionStringForImport = "Server=" + dataSource + ";database=" + dbName.Text + ";Trusted_Connection=Yes";
                lb.AppendText($"\r\nYou may continue working with {dbName.Text}, this database is set for import");
                cnn = Util.Connection(ConnectionStringForImport);
            }
            else
            {
                string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                bool access = Util.GrantAccess(appPath); //Need to assign the permission for current application to allow create database on server (if you are in domain).
                if (access == false)
                {
                    lb.AppendText("\r\n");
                    lb.AppendText("\r\nAdministrative permissions needed for database creation, please run application as administrator");
                    lb.AppendText("\r\n");
                    return;
                }
                str = "CREATE DATABASE " + dbName.Text + " ; ";
                if (cnn == null || cnn.State == ConnectionState.Closed) { cnn = Util.Connection(connectMaster, false); }
                using (SqlCommand myCommand = new SqlCommand(str, cnn))
                {
                    try
                    {
                        myCommand.ExecuteNonQuery();
                        lb.AppendText("\r\n");
                        lb.AppendText($"\r\nDataBase {dbName.Text} is Created Successfully");
                        DataBaseLabel.Text = dbName.Text;
                        
                    }
                    catch (Exception ex)
                    {
                        lb.AppendText("\r\n");
                        lb.AppendText($"\r\nDataBase {dbName.Text} Creation Error ");
                        lb.AppendText("\r\n");
                        lb.AppendText("\r\n" + ex.Message);
                    }
                }

                ConnectionStringForImport = "Server=" + dataSource + ";database=" + dbName.Text + ";Trusted_Connection=Yes";
                string pathStoreProceduresFile = Environment.CurrentDirectory + "\\resources\\script.sql";
              //  string pathStoreProceduresFile = Path.Combine(Environment.CurrentDirectory+"\resources\script.sql");
                try
                {                    
                    string script = File.ReadAllText(pathStoreProceduresFile);

                    using (SqlConnection myConn2 = new SqlConnection(ConnectionStringForImport))
                    {
                        myConn2.Open();
                        // split script on GO command
                        System.Collections.Generic.IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                                 RegexOptions.Multiline | RegexOptions.IgnoreCase);

                        foreach (string commandString in commandStrings)
                        {
                            if (commandString.Trim() != "")
                            {
                                using (var command = new SqlCommand(commandString, myConn2))
                                {
                                    try
                                    {
                                        command.ExecuteNonQuery();
                                    }
                                    catch (SqlException ex)
                                    {
                                        string spError = commandString.Length > 100 ? commandString.Substring(0, 100) + " ...\n..." : commandString;
                                        lb.AppendText("\r\n");
                                        lb.AppendText(string.Format("Please check the SqlServer script.\nFile: {0} \nLine: {1} \nError: {2} \nSQL Command: \n{3}", pathStoreProceduresFile, ex.LineNumber, ex.Message, spError));

                                    }
                                    catch (Exception ex)
                                    {
                                        lb.AppendText("\r\n");
                                        lb.AppendText("\r\n" + ex.Message);
                                    }
                                }
                            }
                        }
                        db = dbName.Text;
                        DataTable t = myConn2.GetSchema("Tables");
                        lb.AppendText($"\r\n");
                        lb.AppendText($"\r\nTables Created in DataBase: {db}");
                        lb.AppendText($"\r\n");

                       Util.DisplayData(t, db);
                    }
                    

                }
                catch (SqlException ex)
                {
                    lb.AppendText("\r\n");
                    lb.AppendText("\r\nMust Run this application as Administrator");
                    lb.AppendText("\r\n" + ex.Message);
                    lb.AppendText("\r\n");

                }
                catch (Exception ex)
                {
                    lb.AppendText("\r\n");
                    lb.AppendText("\r\n" + ex.ToString());
                    lb.AppendText("\r\n");
                }
            }
        }




    }

}

